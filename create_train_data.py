import numpy as np
import os
from pickle import dump
from sklearn.preprocessing import RobustScaler, QuantileTransformer
from help_functions import plot_one_sample, load_n_batches, augment_data, split_data, clean_data

################################################
# Settings
################################################
only_nc = False
augment_noise = True
augment = True
save = True
div = [0.75, 0.9]
from_dir = '~/labeling/batches/batch'
to_dir = '~/labeling/training'
num_batches = 211

modes = ['quant_input', 'unscaled', 'quant_camlid']
mode = modes[0] 

settings = {'only_nc':only_nc, 'augment_noise':augment_noise, 'augment':augment, 'mode':mode}


noise_errors = [[-0.001, 0.001], [-0.0001, 0.0015], [-0.0017, 0.00005], [-0.0001, 0.0001]]
noise = [[-0.01, 0.01], [-0.001, 0.015], [-0.017, 0.0005], [-0.001, 0.001]]

################################################
# Load data
################################################


orig_data = load_n_batches(from_dir, num_batches)
orig_data = np.nan_to_num(orig_data, nan=1e10)


print('Original data', orig_data.shape)


################################################
# Only use nc
################################################
# Extract nc samples
# clean a copy of the samples and use as labels


if only_nc:
    nc_ind = np.where(orig_data[:,0] == 1)
    nc_data = orig_data[nc_ind]
    inds = np.where(nc_data>20)
    before_sample = nc_data[inds[0][0], :]
    # plot_one_sample(before_sample[1:])

    nc_data_y = clean_data(nc_data)

    after_sample = nc_data_y[inds[0][0], :]
    # plot_one_sample(after_sample[1:])
    xtrain, xval, xtest = split_data(div, [nc_data])
    ytrain, yval, ytest = split_data(div, [nc_data_y])
    print('Only nc cases, train: ', xtrain.shape, 'val: ', xval.shape, 'test:', xtest.shape)

################################################
# Use all cases
################################################
# clean a copy of the samples and use as labels
# If augment noise, expand the number of samples of cam and lid error

if not only_nc:

    nc_ind = np.where(orig_data[:,0] == 1)
    nc_data = orig_data[nc_ind]

    error_camera_inds = np.where(orig_data[:,0] == 2) 
    error_camera = orig_data[error_camera_inds]

    error_lidar_inds = np.where(orig_data[:,0] == 3) 
    error_lidar = orig_data[error_lidar_inds]

    error_both_inds = np.where(orig_data[:,0] == 4) 
    error_both = orig_data[error_both_inds]

    
    
    print('Classes, nc: ', nc_data.shape, 'cam:', error_camera.shape, 'lid', error_lidar.shape, 'both', error_both.shape)
    
    nc_data_y = clean_data(nc_data)
    error_camera_y = clean_data(error_camera)
    error_lidar_y = clean_data(error_lidar)
    error_both_y = clean_data(error_both)

    if augment_noise:
        print('Before augmenting noise, cam: ', error_camera.shape, 'lid', error_lidar.shape, 'both', error_both.shape, 
                'y cam: ', error_camera_y.shape, 'lid', error_lidar_y.shape, 'both', error_both_y.shape)

        error_camera = augment_data(error_camera, noise_errors)
        error_lidar = augment_data(error_lidar, noise_errors)
        error_both = augment_data(error_both, noise_errors)

        for i in range(len(noise_errors)-1):
            error_camera_y = np.concatenate((error_camera_y, error_camera_y), axis=0)
            error_lidar_y = np.concatenate((error_lidar_y, error_lidar_y), axis=0)
            error_both_y = np.concatenate((error_both_y, error_both_y), axis=0)


        print('After augmenting noise, cam : ', error_camera.shape, 'lid', error_lidar.shape, 'both', error_both.shape, 
                'y cam: ', error_camera_y.shape, 'lid', error_lidar_y.shape, 'both', error_both_y.shape)


    xtrain, xval, xtest = split_data(div, [nc_data, error_camera, error_lidar, error_both])
    ytrain, yval, ytest = split_data(div, [nc_data_y, error_camera_y, error_lidar_y, error_both_y])

    print('Total data: ', xtrain.shape[0] + xval.shape[0] + xtest.shape[0])
    print('Train, val, and test set: ', xtrain.shape, xval.shape, xtest.shape)


################################################
# Augment train and val data set, use clean as label
################################################

if augment:
    print('Before augment train and val x: ', xtrain.shape, xval.shape, 'y: ', ytrain.shape, yval.shape)
    xtrain = augment_data(xtrain, noise)
    xval = augment_data(xval, noise)
    for i in range(len(noise)-1):
        ytrain = np.concatenate((ytrain, ytrain), axis=0)
        yval = np.concatenate((yval, yval), axis=0)

    
    print('After augment train and val x: ', xtrain.shape, xval.shape, 'y: ', ytrain.shape, yval.shape)
    print('Total data ', xtrain.shape[0] + xval.shape[0] + xtest.shape[0])

################################################
# Scale the input (xdata)
################################################

if mode == 'quant_input':
    print('Applying quant transform on entire input')

    scaler_both = QuantileTransformer()
    scaled_data = np.concatenate((xtrain[:,1:], xval[:,1:]))
    scaled_data = scaler_both.fit_transform(scaled_data)
    
    # plot_one_sample(xtrain[0,1:])

    xtrain[:,1:] = scaled_data[0:xtrain.shape[0],:]
    xval[:,1:] = scaled_data[xtrain.shape[0]:,:]

    # plot_one_sample(xtrain[0,1:])
    # plot_one_sample(ytrain[0,1:])

    if save:
        dump(scaler_both, open(os.path.expanduser(to_dir+'/scaler_both.pkl'), 'wb'))  

    print('Post scale: ', xtrain.shape, xval.shape)

    
if mode == 'unscaled':
    print('No scale performed')

if mode == 'quant_camlid':
    print('Applying separate scale on cam and lid')

    cam_scaler = QuantileTransformer()
    lid_scaler = QuantileTransformer()

    scaled_data = np.concatenate((xtrain[:,1:], xval[:,1:]))
    scaled_cam = cam_scaler.fit_transform(scaled_data[:,:424])
    scaled_lid = lid_scaler.fit_transform(scaled_data[:,424:])

    # plot_one_sample(xtrain[0,1:])
    xtrain[:,1:425] = scaled_cam[0:xtrain.shape[0],:]
    xtrain[:,425:] = scaled_lid[0:xtrain.shape[0],:]
    xval[:,1:425] = scaled_cam[xtrain.shape[0]:,:]
    xval[:,425:] = scaled_lid[xtrain.shape[0]:,:]
    # plot_one_sample(xtrain[0,1:])
    # plot_one_sample(ytrain[0,1:])

    if save:
        dump(cam_scaler, open(os.path.expanduser(to_dir+'/cam_scaler.pkl'), 'wb'))  
        dump(lid_scaler, open(os.path.expanduser(to_dir+'/lid_scaler.pkl'), 'wb'))  

    print('Post scale: ', xtrain.shape, xval.shape)


################################################
## Save data
################################################

if save:
    np.save(os.path.expanduser(to_dir+'/x_train.npy'), xtrain) 
    np.save(os.path.expanduser(to_dir+'/x_val.npy'), xval) 
    np.save(os.path.expanduser(to_dir+'/x_test.npy'), xtest) 
    np.save(os.path.expanduser(to_dir+'/y_train.npy'), ytrain) 
    np.save(os.path.expanduser(to_dir+'/y_val.npy'), yval) 
    np.save(os.path.expanduser(to_dir+'/y_test.npy'), ytest) 

    np.save(os.path.expanduser(to_dir+'/settings.npy'), settings) 

