import numpy as np
import os
import matplotlib.pyplot as plt

def load_n_batches(from_dir, n_batches):
    data = np.load(os.path.expanduser(from_dir+'0.npy'))
    labels = np.load(os.path.expanduser(from_dir+'0_labelled.npy'))
    data[:,2] = labels
    data = data[:,2:]

    for i in range(1, n_batches):
        temp = np.load(os.path.expanduser(from_dir+str(i)+'.npy'))
        labels = np.load(os.path.expanduser(from_dir+str(i)+'_labelled.npy'))
        temp[:,2] = labels
        temp = temp[:,2:]
        data = np.concatenate((data, temp))


    return data

def augment_data(arr, noise):
    exp_arr = arr.copy()
    for i in range(len(noise)-1):
        noise_arr = np.random.normal(noise[i][0], noise[i][1], size=arr.shape)
        noise_arr[:,0] = 0
        noise_arr = noise_arr + arr
        exp_arr = np.concatenate((exp_arr, noise_arr), axis=0)
    noise_arr = np.random.normal(noise[-1][0], noise[-1][1], size=exp_arr.shape)
    noise_arr[:,0] = 0
    noise_arr = noise_arr + exp_arr
    exp_arr = np.concatenate((exp_arr, noise_arr), axis=0)
    return exp_arr

def split_data(div, arr):
    train = []
    val = []
    test = []
    for i in range(len(arr)):
        n = arr[i].shape[0]
        temp = np.split(arr[i], [int(n*div[0]), int(n*div[1])])
        train.append(temp[0])
        val.append(temp[1])
        test.append(temp[2])

    train = np.vstack(train)
    val = np.vstack(val)
    test = np.vstack(test)
    return train, val, test


def clean_data(arr):
    clean_arr = arr.copy()
    for row in range(arr.shape[0]):
        ins = np.where(arr[row,:] > 20) 
        for j in range(len(ins[0])):
            clean_arr[row, ins[0][j]] = clean_arr[row, ins[0][j]-1]
   
    return clean_arr

def plot_one_sample(sample):
    rads_lid = np.arange(-3.1241390705108643, 3.1415927410125732+0.01745329238474369 ,  0.01745329238474369)
    rads_cam = np.arange(-0.7669617533683777, 0.7851662635803223+0.0036693334113806486 ,  0.0036693334113806486)


    fig, ax = plt.subplots(subplot_kw={'projection': 'polar'}, figsize=(12,10))
    ax.set_ylim(0,4)
    plt.subplots_adjust(bottom=0.2, top=0.8)


    plt.plot(rads_cam, sample[:424], lw=2)
    plt.plot(rads_lid, sample[424:], lw=2)

    plt.show()

# for i in range(len(clean_data)):
#     for row in range(clean_data[i].shape[0]):
#         ins = np.where(clean_data[i][row,:] > 20)     ## all indexes out of range
#         for j in range(len(ins[0])):
#             clean_data[i][row, ins[0][j]] = clean_data[i][row, ins[0][j]-1]
# print('ins', ins)
# print('error lidar', error_lidar[-1, ins[0]])
# print('clean lidar', clean_data[2][-1, ins[0]])

# from_dir = '~/labeling/batches/batch'
# load_n_batches(from_dir, 4)
# arr = np.zeros((10,4))
# arr2 = np.ones((10,4))
# print('arr',arr.shape)
# div = [0.75, 0.9]
# train, val, test = split_data(div, [arr])
# print(train.shape, val.shape, test.shape)

