import numpy as np
import os
from load_npy_data import load_npy_data

################################################
# Load all data
################################################

data = load_npy_data()

inf = np.where(data > 1000)
data[inf] = np.nan

batch_size = 200

num_batch =  round(data.shape[0] / batch_size)

for i in range(num_batch):
    if i == num_batch-1:
        batch = data[i*batch_size:, :]
    else:
        batch = data[i*batch_size:(i+1)*batch_size, :]
    # print("i,batch.shape",i, batch.shape)

    batch_number = i
    batch_name = "batch"+str(batch_number)
    np.save(os.path.expanduser('~/labeling/batches/'+batch_name),batch)


