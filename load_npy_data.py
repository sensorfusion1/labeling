import numpy as np
from math import ceil, floor
import os


def load_npy_data():
    folders = ['Aa', 'Ab', 'Ac', 'Ba', 'Bb', 'Bc', 'Ca'] #Cb and Cc special
    cases = ['nc1', 'nc2', 'nc3', 'tc1', 'tc2']

    data = np.empty((1,787))

    which_scene = []
    counter = 0
    which_batch = -1
    for folder in folders:
        for case in cases:
            label_detect = np.load(os.path.expanduser('~/labeling/npydata/'+folder+'/label_'+case+'.npy')) 
            cam = np.load(os.path.expanduser('~/labeling/npydata/'+folder+'/cam_'+case+'.npy'))
            lid = np.load(os.path.expanduser('~/labeling/npydata/'+folder+'/lid_'+case+'.npy'))

            conc_set = np.concatenate((label_detect, cam, lid), axis=1)
            data = np.concatenate((data, conc_set), axis=0)
            
            counter += 1
            which_batch += float(len(label_detect))/200.0
            which_scene.append((folder,case,len(label_detect),which_batch))
    
    

    # Remove top empty row
    data = data[1:,:]


    foldersC = ['Cb', 'Cc']
    casesC = ['nc1', 'nc2', 'nc3']

    for folder in foldersC:
        for case in casesC:
            label_detect = np.load(os.path.expanduser('~/labeling/npydata/'+folder+'/label_'+case+'.npy')) 
            cam = np.load(os.path.expanduser('~/labeling/npydata/'+folder+'/cam_'+case+'.npy'))
            lid = np.load(os.path.expanduser('~/labeling/npydata/'+folder+'/lid_'+case+'.npy'))

            conc_set = np.concatenate((label_detect, cam, lid), axis=1)
            data = np.concatenate((data, conc_set), axis=0)

            counter += 1
            which_batch += float(len(label_detect))/200.0
            which_scene.append((folder,case,len(label_detect),which_batch))
    print('Number of samples', data.shape)
    # print(which_scene)
    return data



def load_npy_case(folder, case):

    data = np.empty((1,787))

    label_detect = np.load(os.path.expanduser('~/labeling/npydata/'+folder+'/label_'+case+'.npy')) 
    cam = np.load(os.path.expanduser('~/labeling/npydata/'+folder+'/cam_'+case+'.npy'))
    lid = np.load(os.path.expanduser('~/labeling/npydata/'+folder+'/lid_'+case+'.npy'))

    conc_set = np.concatenate((label_detect, cam, lid), axis=1)
    data = np.concatenate((data, conc_set), axis=0)

    # Remove top empty row
    data = data[1:,:]

    print('Number of samples', data.shape)

    return data

def load_npy_scene(scene):
    # ###############################################
    # Load the scene samples into the 'data' variable
    # ###############################################
    folders = [str(scene)+'a', str(scene)+'b', str(scene)+'c']
    cases = ['nc1', 'nc2', 'nc3', 'tc1', 'tc2']

    data = np.empty((1,787))

    for folder in folders:
        for case in cases:
            if any(f == folder for f in ['Cb','Cc']) and any(c == case for c in ['tc1','tc2']):
                pass
            else:
                label_detect = np.load(os.path.expanduser('~/labeling/npydata/'+folder+'/label_'+case+'.npy')) 
                cam = np.load(os.path.expanduser('~/labeling/npydata/'+folder+'/cam_'+case+'.npy'))
                lid = np.load(os.path.expanduser('~/labeling/npydata/'+folder+'/lid_'+case+'.npy'))

                conc_set = np.concatenate((label_detect, cam, lid), axis=1)
                data = np.concatenate((data, conc_set), axis=0)
                
    # Remove top empty row
    data = data[1:,3:]


    # ################################################
    # Load the scene labels into the 'labels' variable
    # ################################################
    batch_size = 200
    scenes_num = {'A':14415, 'B':14526, 'C':13467} # dict with sample no. of each scene
    batch_nums = {'A':{'start':0,'end':ceil(scenes_num['A']/batch_size)}, 'B':{'start':ceil(scenes_num['A']/batch_size-1),'end':ceil((scenes_num['A']+scenes_num['B'])/batch_size)},
                    'C':{'start':ceil((scenes_num['A']+scenes_num['B'])/batch_size-1),'end':floor((scenes_num['A']+scenes_num['B']+scenes_num['C'])/batch_size)}} 
    batch_ind = {'A':{'start':0, 'end':scenes_num['A']%batch_size}, 'B':{'start':scenes_num['A']%batch_size, 'end':(scenes_num['A']+scenes_num['B'])%batch_size},
                    'C':{'start':(scenes_num['A']+scenes_num['B'])%batch_size, 'end':(scenes_num['A']+scenes_num['B']+scenes_num['C'])%batch_size+200}}

    labels = np.empty((1,1))
    for i,batch in enumerate(range(batch_nums[scene]['start'],batch_nums[scene]['end'])):
        # print("i",i,"batch",batch,"start",batch_ind[scene]['start'],"end",batch_ind[scene]['end'])
        temp = np.load(os.path.expanduser('~/labeling/batches/batch'+str(batch)+'_labelled.npy'))
        if batch == batch_nums[scene]['start']:
            labels = np.append(labels,temp[batch_ind[scene]['start']:])
        elif batch == batch_nums[scene]['end']-1:
            labels = np.append(labels,temp[:batch_ind[scene]['end']])
        else:
            labels = np.append(labels,temp)
    
    # Remove top empty row and save scene A's labels
    labels = labels[1:scenes_num[scene]+1]-1

    print('Scene', scene,'. Number of samples:', data.shape,'. Number of labels:', len(labels))

    return labels, data


if __name__ == "__main__":
    load_npy_data()